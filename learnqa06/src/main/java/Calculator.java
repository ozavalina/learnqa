import java.util.Scanner;

public class Calculator {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите строку в формате: ЧИСЛО ОПЕРАЦИЯ ЧИСЛО.\nОперация может быть указана одним из способов: плюс/+, минус/-, делить//, умножить/*.\nДля выхода введите ВЫХОД");
        String userText = scanner.nextLine();
        while (!"выход".equalsIgnoreCase(userText)) {
            performOperation(userText);
            userText = scanner.nextLine();
        }
    }

    public static int[] checkAndGetAandB(String userText) {
        String[] words;
        int[] ab = new int[2];
        words = userText.split(" ");
        if (words.length == 3) {
            try {
                ab[0] = Integer.parseInt(words[0]);
            } catch (NumberFormatException e) {
                System.out.println("Первое значение - не число. Введите строку в формате: ЧИСЛО ОПЕРАЦИЯ ЧИСЛО.");
                e.printStackTrace();
            }
            try {
                ab[1] = Integer.parseInt(words[2]);
            } catch (NumberFormatException e) {
                System.out.println("Второе значение - не число. Введите строку в формате: ЧИСЛО ОПЕРАЦИЯ ЧИСЛО.");
                e.printStackTrace();
            }
        } else {
            System.out.println("1Строка не соответствует формату: ЧИСЛО ОПЕРАЦИЯ ЧИСЛО. Введите строку в указанном формате.");
        }
        return ab;
    }

    public static String getOperator(String userText) {
        String[] words;
        String operation = "";
        try {
            words = userText.split(" ");
            operation = words[1].toLowerCase();
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("2Строка не соответствует формату: ЧИСЛО ОПЕРАЦИЯ ЧИСЛО. Введите строку в указанном формате.");
            e.printStackTrace();
        }
        return operation;
    }

    public static void performOperation(String userText) {
        switch (getOperator(userText)) {
            case ("+"):
            case ("плюс"):
                add(checkAndGetAandB(userText));
                break;
            case ("-"):
            case ("минус"):
                subtract(checkAndGetAandB(userText));
                break;
            case ("*"):
            case ("умножить"):
                multiply(checkAndGetAandB(userText));
                break;
            case ("/"):
            case ("делить"):
                divide(checkAndGetAandB(userText));
                break;
            default:
                System.out.println("Оператор введён неверно. Операция может быть указана одним из способов: плюс/+, минус/-, делить//, умножить/*.");
                break;
        }
    }

    public static void add(int[] ab) {
        System.out.println(ab[0] + ab[1]);
    }

    public static void subtract(int[] ab) {
        System.out.println(ab[0] - ab[1]);
    }

    public static void multiply(int[] ab) {
        System.out.println(ab[0] * ab[1]);
    }

    public static void divide(int[] ab) {
        int c = 0;
        try {
            c = ab[0] / ab[1];
        } catch (ArithmeticException e) {
            System.out.println("На ноль делить нельзя");
            e.printStackTrace();
            throw e;
        }
        System.out.println(c);
    }
}
