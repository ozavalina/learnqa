/**
 * Дженерики (обобщения) - механизм шаблонизации, позволяющий работать с различными типами данных без изменения их описания.
 * Т.е. дженерики помогают писать универсальный, переиспользуемый код, который одинаково будет работать со значениями разных типов.
 * Основная цель дженериков - обеспечить типобезопасность при работе с разными типами данных.
 *
 * @param <T> - обощенный тип данных. Вместо него при создании объекта дженерика необходимо будет подставить требуемый ссылочный тип.
 */

public class MyStack<T> {
    T[] elements = (T[]) new Object[2];
    int index = 0;

    //класть
    public void push(T element) {
        if (index < elements.length) {
            elements[index] = element;
            index++;
        } else {
            System.out.println("Стек заполнен");
        }
    }

    //брать
    public T pop() {
        T element;
        index--;
        if (index >= 0) {
            element = elements[index];
            elements[index] = null;
        } else element = null;
        return element;
    }
}
